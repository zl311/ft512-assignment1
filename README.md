# ft512-assignment1

### Paper1: 'No Silver Bullet: Essence and Accident in Software Engineering'.
Keywords: software, breakthrough, difficulties

The author of the essay "No Silver Bullet" begins by giving us a 
quick overview of the present state of software development, and then adds, 
"We must remark that the abnormality is not 
that software advancement is so sluggish, 
but that computer hardware progress is so fast." 

the author categorizes the issues into two types: essential and unintentional. 
He covers numerous techniques for stimulating development, including complexity, conformance, changeability, and invisibility, while discussing crucial challenges.
The author begins by discussing how humans overcome unintentional challenges in the past.
Object-oriented programming, artificial intelligence, expert systems, and "Automatic" programming are some of the technological innovations that are frequently pushed as possible silver bullets to help us achieve remarkable breakthroughs in the software sector.

### Paper2 'They Write The Right Stuff'
keyword: software, error, group, process

Software may power the post-industrial world, but the creation of software remains a pre-industrial trade. The situation is so severe, a few software pioneers from companies such as Microsoft have broken away to teach the art of software creation. The group's most important creation is not the perfect software they write it's the process they invented that writes the perfect software. 

The author has a clear definition and understanding of data and software, and tells us that recording errors is a very important process of software. And it tells us to avoid the source of the error ahead of time. The disadvantage of the article is that it does not provide too many solutions

### Video 'The History (and Future) of Software', Booch
keyword: hardware, software, development, future, evolution

Computing hardware exists in the physical world: we can see, touch, move, and analyze these things. The software that gives that hardware life exists in an ethereal realm that is generally concealed from view. Despite this, the narrative of software is equally as intriguing as the story of hardware: both realms are full with tales of ambition, ingenuity, creativity, vision, greed, and serendipity. We can call the epochs of software in the same way as we can distinguish unambiguous periods in the history of computing hardware.

Grady Booch describes the exploration and evolution of computers. He looks at interesting examples of software and observes trends in software change. But the future is unpredictable and needs to be confirmed by more examples and demonstration



#### 2. A link to GitLab repo
https://gitlab.oit.duke.edu/zl311/ft512-assignment1

#### 3. Notes on time took for each part ('Read', 'Watch', 'Do')
Read: 4h

Watch: 3h

Do/coding: 12h

